FROM registry.fedoraproject.org/fedora-toolbox:36

# This is specific for X-ray analysis
COPY xray-packages /
RUN dnf install -y $(<xray-packages)
run rm /xray-packages

# Not everything is available as a fedora .rpm package, some
# stuff needs to be installed from pip.
RUN pip install \
        silx \
	pyqt5 \
	fabio \
	pyFAI[gui] \
        xrayutilities \
        spec2nexus \
        python-nexus

RUN SPECFILE_USE_GNU_SOURCE=1 pip install PyMca5

ENV QT_STYLE_OVERRIDE=Adwaita-dark

#
# Below this point everything is play-nice with toolbox
# (see https://containertoolbx.org/ :-)
#
# The LABEL com.github.containers.toolbox="true" is essential,
# the rest is shamelessly copied from https://github.com/containers/toolbox
# for good measure.
#

ENV NAME=xan-fedora-toolbox VESION=36

LABEL   com.github.containers.toolbox="true" \
	maintainer="Florin Boariu <florin.boariu.hzb@selfmx.net>"

RUN dnf clean all

ENTRYPOINT []
