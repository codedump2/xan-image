#!/bin/bash

IMAGE=${IMAGE:=registry.gitlab.com/codedump2/xan-image:latest}

# which security label to use (everywhere the same?)
MOUNT_LABEL="z"

# need to avoid mount $HOME twice if it's PWD
MOUNT_LIST="\
    -v /media:/media:$MOUNT_LABEL \
    -v $HOME:$HOME:$MOUNT_LABEL \
    -v $XDG_RUNTIME_DIR:$XDG_RUNTIME_DIR:$MOUNT_LABEL \
"

if [ "$PWD" != "$HOME" ]; then
    MOUNT_LIST="$MOUNT_LIST -v $PWD:$PWD:$MOUNT_LABEL"
fi

podman run \
      -ti \
      --rm \
      -e DISPLAY \
      -v /tmp/.X11-unix:/tmp/.X11-unix \
      -e XDG_RUNTIME_DIR=$XDG_RUNTIME_DIR \
      --device=/dev/dri:/dev/dri \
      --security-opt seccomp=unconfined \
      --security-opt label=type:container_runtime_t \
      --security-opt label=disable \
      --privileged \
      -e HOME=$HOME \
      -e HOSTNAME="x-ray" \
      -e PS1="\u 🔬\h: \w \$ " \
      $MOUNT_LIST \
      --userns=keep-id \
      $IMAGE $@
