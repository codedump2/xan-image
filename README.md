X-ray analysis toolbox image
============================

What is this?
-------------

For those unfamiliar with "images", there's a decent
[introduction over at freeCodeCamp](https://www.freecodecamp.org/news/a-beginner-friendly-introduction-to-containers-vms-and-docker-79a9e3e119b/) about Docker, images, container etc. Essentially,
it's an isolated (Linux) enviroment.

This particular image contains a number of tools needed for X-ray
data analysis, based on a Fedora 35 Linux distribution. Here's a
non-exhaustive list of tools within:

  - The [NexusFormat](https://nexusformat.org) tool suite
  - [spec2nexus](https://github.com/prjemian/spec2nexus), a utility for conveniend data conversion
  - [ESRF tools and libraries](https://silx.org), for example:
    - [PyMCA](http://pymca.sourceforge.net/)
    - [pyFAI](https://github.com/silx-kit/pyFAI)
    - [FabIO](https://github.com/silx-kit/fabio)
  - [xrayutilities](https://xrayutilities.sourceforge.io/), a Python library for data analysis
  - [fitky](https://fityk.nieto.pl/), a peak fitting tool
  - [SageMath](https://www.sagemath.org/), the famous computer Python algebra package
  - [Spyder](https://www.spyder-ide.org/), a Python IDE known for its interactive capabilities
  - [Jupyter](https://jupyter.org/), the self-styled "next-generation" notebook interface
  - [ipython](https://ipython.org/), a Python shell for interactive computing (essentially Jupyter at its core)
  - Many Python 3 libraries and their dependencies, most prominently:
    MatPlotLib, Qt5/PyQt5, h5py, SciPy, NumPy, NumXpr, PyOpenCL, Cython
  - Many goodies like emacs, git, the Adwaita themes (including the dark version), the
    GNU compiler suite, the [starthip](https://starship.rs/) prompt


Obtaining the image
-------------------

There are two ways how you can obtain the usable image: the first is create the image
on your own computer using [Docker](https://www.docker.com/)
or [Podman](https://podman.io). The othre is pulling a finished image from
[the author's public GitLab account](https://gitlab.com/codedump2/xan-image).

### Creating from source

To create your own image, follow these steps. I'm describing this using the `podman`
command (Podman being a very lightweight alternative to Docker), but the command
line is the same. If you're new to this, and are using docker, just replace every
`podman` command in the following by `docker`, using exactly the same command line
options:

1. Download the sources, either from [GitLab.com](https://gitlab.com/codedump2/xan-image)
   or from the [Uni Potsdam GitLab server](https://git.udkm.physik.uni-potsdam.de/boariu/xan-image).
   (The versions should be the same, I'm trying to keep them in sync; if in doubt,
   double-check yourself):
   ```
       $ git clone https://gitlab.com/codedump2/xan-image
   ```
   
2. Start building the image from Dockerfile:
   ```
       podman build -f xan-image/Dockerfile -t xan-image
   ```
   
### Pulling from Gitlab.com

The project has an automated CI/CD setup which automatically builds the image from the
`release` branch on any change, if the underlying GitLab installation supports that.
This is for example the case over at Gitlab.com, so you can also directly pull
the image from there:

```
    $ podman pull registry.gitlab.com/codedump2/xan-image:release
```


Running the image using Docker or Podman
-----------------------------------------

The regular way of running images is by `podman run`. This, for example, would open
a Bash shell inside the container:
```
    $ podman run -ti --rm xan-image bash
```

You can also single-shot specialized tools and commands instead, e.g. PyMCA:
```
    $ podman run -ti --rm xan-image pymca
```

The problem is that like this, the container completely isolated from your regular
host PC. It would likely not have any access to your experimental data, network
resources, etc. This is why a better idea is to poke holes in this perfect isolation
and share some resources with your host, for example:
  - share a data directory
  - share your custom code directory
  - share your user identity
  - [optional] share your current workind directory
  - [optional] share your home directory with all your files
  - [optional] share the X11 devices and/or sockets
  - [optional] share the same network namespace
  - [optional] share the `/media` directory so you can easily access thumbdrives
  
Considering this, here's a more useful `podman run` command, assuming that your
data directory on the host is `$HOME/xraydata`, and the code you're using
is `$HOME/xraycode`:
```
    $ podman run -ti --rm \
             -e DISPLAY \
             -v /tmp/.X11-unix:/tmp/.X11-unix \
             --device=/dev/dri:/dev/dri \
             -e HOME=$HOME --userns=keep-id \
             -v /data:$HOME/xraydata:z \
             -v /code:$HOME/xraycode:z \
             xan-image \
             /bin/bash

```

The supplied `xan-image/run.sh` script does something similarly. It has been
tested to work with Debian 11. Not all options therein may be necessary on
your particular setup, but at least it's a starting point.

But note that the preferred way of working with Xan-Image is *not* by using
Podman / Docker directly, but by going through the Toolbx-utility.


Running the image using Toolbx
------------------------------

The [Toolbx](https://containertoolbx.org/) utility (formerly known as Toolbox)
is, in its essence, a fairly sophisticated Go script wrapped around Podman.
Toolbx takes over the tasks [described above](#running-the-image-using-docker-or-podman)
of "poking holes" in the separation between your host and the container,
similarly to the `./xan-image/run.sh` script, but does so in a *much* more
robust, seamless and well thought-of manner. Additionally, Toolbx allows you
to modify the `xan-image` to persistently adapt the data analysis container
to your own needs (this isn't possible using Podman directly).

Toolbx was originally developed for, and is natively available in, the
[Fedora Silverblue](https://silverblue.fedoraproject.org/) Linux distribution.
But it has since evolved into a product of its own and is available for
a wide range of other distributions, including for exampe Debian.

Once you have Toolbx installed, this is how you go about creating
your own local toolbox, based on the `xan-image`:
```
    $ toolbox create xan -i registry.gitlab.com/codedump2/xan-image:latest
```

You can feel free to use another image, for example the one you
[created locally](#creating-from-source).

Then go ahead and use it with `toolbox enter`:

```
    $ toolbox enter xan
	◆[user@toolbox] $ pymca ...
```

Enjoy! :-)


Bugs & Feedback
===============

This being F/OSS, if you break it, you get to keep both halves. Post bugs
and suggestions in the [Gitlab issue manager](https://gitlab.com/codedump2/xan-image/-/issues),
but no promisses.

Every once in a while, I'm adding new tools and restructuring as I go
along. It may pay out to check back in regularly.
